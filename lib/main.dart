import 'package:flutter/material.dart';
import 'dart:async';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:random_words/random_words.dart';
// import 'package:flutter_icons/flutter_icons.dart';
import 'package:auto_size_text/auto_size_text.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'NoisIO',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var urlforfree = "stefanos";
  var word = "Welcome!";
  bool isLoading = true;

  Timer _timer;

  final Completer<WebViewController> _controller = Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
  }

  void updateView(controller, context) {
    print(DateTime.now());
    setState(() {
      word = generateWordPairs().take(1).first.toString();
    });
    controller.data.loadUrl('https://duckduckgo.com?q=!ducky+$word&kp=1');
    Scaffold.of(context).showSnackBar(
      SnackBar(duration: Duration(seconds: 5), content: Text('Searching for $word.')),
    );
  }

  _changeWebSite(String url) {
    return FutureBuilder<WebViewController>(
      future: _controller.future,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        if (controller.hasData) {
          return Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: Container(
                    child: Ink(
                      decoration: ShapeDecoration(
                        color: Colors.red,
                        shape: Border(),
                      ),
                      child: IconButton(
                        // backgroundColor: Colors.red,
                        color: Colors.black,
                        icon: Icon(Icons.stop, size: 50),
                        // label: Text('STOP'),
                        onPressed: () async {
                          // var url = await controller.data.currentUrl();
                          this._timer.cancel();
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                                duration: Duration(seconds: 5),
                                content: Text('Stopped. To restart click on the go button.')),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Center(
                  child: Text("Hello there."),
                ),
              ),
              Expanded(
                flex: 1,
                child: SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: Container(
                    child: Ink(
                      decoration: ShapeDecoration(
                        color: Colors.green,
                        shape: Border(),
                      ),
                      child: IconButton(
                        // backgroundColor: Colors.green,
                        // label: Text("GO"),
                        icon: Icon(Icons.play_arrow, size: 50),
                        color: Colors.black,
                        splashColor: Colors.green,
                        // child: Text('GO'),
                        onPressed: () async {
                          // Scaffold.of(context).showSnackBar(
                          //   SnackBar(
                          //       duration: Duration(seconds: 5),
                          //       content: Text('Starting your journey! Timer is set to 20 seconds.')),
                          // );
                          updateView(controller, context);
                          this._timer =
                              Timer.periodic(Duration(seconds: 20), (timer) => updateView(controller, context));
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }
        return Container();
      },
    );
    // return FutureBuilder<WebViewController>(
    //   future: _controller.future,
    //   builder: (BuildContext context, AsyncSnapshot<WebViewController> controller) {
    //     print('uyo');
    //     if (controller.hasData) {
    //       print('yo');
    //       setState(() {
    //         urlforfree = 'e';
    //       });
    //     }
    //     return Container();
    //   },
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 8,
              child: WebView(
                initialUrl: "https://www.privacytools.io/",
                javascriptMode: JavascriptMode.unrestricted,
                onPageFinished: (_) {
                  setState(() {
                    isLoading = false;
                  });
                },
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
              ),
            ),
            isLoading ? Center(child: CircularProgressIndicator()) : Container(),
            Expanded(flex: 1, child: _changeWebSite('url')
                // child: Container(
                //   margin: new EdgeInsets.symmetric(horizontal: 80.0, vertical: 20.0),
                //   child: Center(
                //       child: AutoSizeText(
                //           "Let's throw some shoes around! Press on the GO button to start mudding the waters.")),
                ),
          ],
        ),
      ),
    );
  }
}

// class MyWebView extends StatefulWidget {
//   final String selectedUrl;
//   MyWebView({
//     Key key,
//     @required this.selectedUrl,
//   }) : super(key: key);

//   @override
//   _MyWebViewState createState() => _MyWebViewState(selectedUrl);
// }

// class _MyWebViewState extends State<MyWebView> {
//   String selectedUrl;
//   _MyWebViewState(this.selectedUrl);
//   final Completer<WebViewController> _controller = Completer<WebViewController>();

//   void _changeWebSite(String url) {
//     print(url);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: WebView(
//       initialUrl: selectedUrl,
//       javascriptMode: JavascriptMode.unrestricted,
//       onPageFinished: _changeWebSite,
//       onWebViewCreated: (WebViewController webViewController) {
//         _controller.complete(webViewController);
//       },
//     ));
//   }
// }
